# 安大通

安大通是一个立足于安徽大学，由安徽大学学生自发开发的集电子课表、~~校内新闻~~、~~电子图书馆~~、~~成绩查询~~、~~考试查询~~等实用功能于一体的App

(ps: ~~功能~~代表已取消)

## 技术栈
安大通 App 采用 MVVM 架构，使用全新的 Compose UI框架开发。开发语言以 Kotlin 为主，有少量 Java 代码。

## 软件展示
软件部分界面效果如下：
(ps: 并非最新版本)
<div>
    <img src="pic/login_page.png" alt="登录页" width="32%">
    <img src="pic/schedule_page.png" alt="课程表" width="32%">
    <img src="pic/home_page.png" alt="主页" width="32%">
</div>
<div>
    <img src="pic/tools_page.png" alt="工具页" width="32%">
    <img src="pic/grade_page.png" alt="成绩查询" width="32%">
    <img src="pic/exam_page.png" alt="考试查询" width="32%">
</div>
